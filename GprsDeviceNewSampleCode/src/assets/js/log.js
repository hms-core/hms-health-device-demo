/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export default {
    debug(msg, ...info) {
        this.print("DEBUG", msg, ...info);
    },
    info(msg, ...info) {
        this.print("INFO", msg, ...info);
    },
    warn(msg, ...info) {
        this.print("WARN", msg, ...info);
    },
    error(msg, ...info) {
        this.print("ERROR", msg, ...info);
    },

    print(type, msg, ...info) {
        console.log(msg, ...info);
        msg = JSON.stringify(msg);
        info.forEach(element => {
            msg = msg + ", " + JSON.stringify(element);
        });
        
        try {
            switch(type) {
                case "DEBUG":
                    window.BasicLib.logger.debug(msg, "[H5 Project]");
                break;
                case "INFO":
                    window.BasicLib.logger.info(msg, "[H5 Project]");
                break;
                case "WARN":
                    window.BasicLib.logger.warn(msg, "[H5 Project]");
                break;
                case "ERROR":
                    window.BasicLib.logger.error(msg, "[H5 Project]");
                break;
            }
        } catch (e) {
            console.log(type, msg);
        }
    }

}
