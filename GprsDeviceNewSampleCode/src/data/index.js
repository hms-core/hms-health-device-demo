/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import scopes from './scopes';
import request from './request';

// 登录授权接口
export function login() {
    return new Promise((resolve, reject) => {
        let useAuthCode = false; // 是否使用AuthCode模式，厂商需要开启
        BasicLib.account.signIn({
            appid: globalData.APP_ID, // 联盟上申请到的APPID
            useAuthCode,
            scopes,  // 权限列表
            redirectUrl: 'xxxxxxxxx'   // 开发者联盟上有设置回调地址时必填，值为在开发者联盟上申请appid时设置的回调地址
        })
        .then(res => {
            log.info('login success', res);
            // AT模式
            if(!useAuthCode){
                window.AT = res.accessToken;
                resolve(true);
                return;
            }

            // AuthCode模式
            let authCode = res.serverAuthCode ?? res.AuthorizationCode; // 未安装hms三方手机字段名为AuthorizationCode
            request.checkScopes(authCode).then( (accessToken, isAuthed) => {
                window.AT = accessToken;
                resolve(isAuthed);
            }).catch(err => {
                reject(err);
            })
        })
        .catch(err => {
            log.error("login error:", err);
            reject(err);
        });
    })
}

export function bindByKit(prodId) {
    return request.bindByKit(prodId);
}