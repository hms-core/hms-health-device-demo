/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import axios from 'axios';
import qs from 'qs';
import scopes from './scopes';

axios.defaults.timeout = 10000;
export default {
    checkScopes(code) {
        return new Promise((resolve, reject) => {
            let param = {
                grant_type: 'authorization_code',
                code, // 授权码Code，只有5分钟有效期，并且用完一次就会失效
                client_id: globalData.APP_ID, // 应用的App ID
                client_secret: '****************************************************************', // 应用的Secret Key，在开发者联盟上查看
                redirect_uri: 'xxxxxxxxxxxxxx' // agc网站上申请的回调url，没有可写 hms://redirect_uri
            }

            // 使用授权码Code获取AT
            axios.post('xxxxxxxxxxxxxxxxx',
                    qs.stringify(param), {
                        headers: {
                            "Content-Type": "application/x-www-form-urlencoded"
                        }
                    })
                .then(response => {
                    console.log('request token response', response);
                    // 响应结果，包含Access Token、scope等
                    let accessToken = JSON.parse(JSON.stringify(response)).access_token;
                    // 拿到用户已授权的权限scope，字符串格式，拆分成已授权数组
                    let authorizedScopes = JSON.parse(JSON.stringify(response)).scope.split(' ');
                    // 遍历调用signIn传入的scopes数组，在已授权列表中查找，返回结果
                    let isAuthed = scopes.every(item => {
                        return authorizedScopes.includes(item);
                    })
                    resolve(accessToken, isAuthed);
                })
                .catch(err => {
                    console.log('request token error', err);
                    reject(err);
                })
        })
    },
    bindByKit(prodId) {
        return new Promise((resolve, reject) => {
            let params = {
                "dataTypes": [
                    "xxxxxxxxxx"
                ],
                "appInfo": {
                    "appName": "xxxxxxxxxx",
                    "desc": "xxxxxxxxxx",
                    "appVersion": "xxxxxxxxxx"
                },
                "deviceInfo": {
                    "manufacturer": "xxxxxxxxxx",
                    "modelNum": "xxxxxxxxxx",
                    "devType": "xxxxxxxxxx",
                    "uniqueId": window.device_sn,
                    "prodId": prodId,
                    "version": "xx"
                }
            }
            let headers = {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": "Bearer " + window.AT
            }
            // 使用授权码Code获取AT	
            axios.post('xxxxxxxxxxxxxxxxxxxxx',
                    params, {
                        headers
                    })
                .then(response => {
                    resolve(response);
                })
                .catch(err => {
                    console.log('request token error', err);
                    if (err.response) {
                        reject(err.response.data.error);
                    } else {
                        reject(err);
                    }
                })
        });
    }
}