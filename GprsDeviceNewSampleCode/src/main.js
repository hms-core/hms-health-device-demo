/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Vue from 'vue'
import App from './App.vue'
import router from './router/index.js'
import BasicLib from '@huawei/h5pro-basic-lib';
import jsInjection from '@/assets/js/jsInjection.js';
import log from './assets/js/log.js';
import VueI18n from 'vue-i18n';
import globalData from '@/assets/js/global.js';
import "@huawei/h5pro-hilink";

window.BasicLib = BasicLib;
window.globalData = globalData;
window.log = log;

Vue.use(VueI18n);
Vue.prototype.jsInjection = jsInjection;

// i-18n
const messages = {
    'bo_CN': require('./assets/i18n/lang/bo_CN'),
    'en_US': require('./assets/i18n/lang/en_US'),
    'zh_CN': require('./assets/i18n/lang/zh_CN'),
    'zh_HK': require('./assets/i18n/lang/zh_HK'),
    'zh_TW': require('./assets/i18n/lang/zh_TW'),
    'ug_CN': require('./assets/i18n/lang/ug_CN'),
}
const defaultLocale = 'zh_CN'
var locale = jsInjection.getLocale() // 语言标识
Vue.prototype.$rtl = ['ug_CN', 'ug'].includes(locale);  //  ug维吾尔语为RTL语言，需要做镜像处理

var localeFounded = false
for (var prop in messages) {
    if (locale === prop) {
        localeFounded = true
        break
    }
}
const defaultLanguageConfig = {
    'en': 'en_US',
    'es': 'es_ES',
    'pt': 'pt_BR',
    'sr': 'sr_MS'
}
// 只根据语言进行匹配
if (!localeFounded) {
    var language = locale.split('_')[0]
    // 遍历以上4种存在默认语言的语种
    for (prop in defaultLanguageConfig) {
        if (prop === language) {
            locale = defaultLanguageConfig[prop]
            localeFounded = true
            break
        }
    }
    // 从messages中遍历查找语言
    for (prop in messages) {
        if (localeFounded) {
            break
        }
        var configLanguage = prop.split('_')[0]
        if (configLanguage === language) {
            locale = prop
            localeFounded = true
        }
    }
}

// 仍然没有找到匹配的语言，使用默认语言
if (!localeFounded) {
    locale = defaultLocale
}
log.info('locale = ' + locale)
const i18n = new VueI18n({
    locale: locale,
    messages: messages
})

async function obtainInfo() {
    // 获取设备信息，包括是否是暗色模式
    if (process.env.NODE_ENV == "development") {
        window.isDark = false;
        window.topHeight = 0;
    } else {
        const currentDeviceInfo = await BasicLib.util.deviceInfo();
        window.isDark = currentDeviceInfo.isDarkTheme;
        window.topHeight = currentDeviceInfo.topLayout;
    }
    // 隐藏标题栏
    try {
        BasicLib.container.setImmerse({
            isImmerse: true,
            isStatusBarTextBlack: true
        });
    } catch (error) {
        console.error(error);
    }

    Vue.config.devtools = true;
    new Vue({
        render: h => h(App),
        router,
        i18n
    }).$mount('#app');
}

obtainInfo();