/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import scopes from './scopes';
import request from './request';

// 登录授权接口
export function login() {
    return new Promise((resolve, reject) => {
        let useAuthCode = true;
        BasicLib.account.signIn({
            appid: globalData.APP_ID, // 联盟上申请到的APPID
            useAuthCode,
            scopes,  // 权限列表
            redirectUrl: 'hms://redirect_uri'   // 开发者联盟上有设置回调地址时必填，值为在开发者联盟上申请appid时设置的回调地址
        })
        .then(res => {
            log.info('login success', res);
            // AT模式
            if(!useAuthCode){
                resolve();
                return;
            }

            // AuthCode模式
            let authCode = res.serverAuthCode || res.AuthorizationCode; // 未安装hms三方手机字段名为AuthorizationCode
            request.checkScopes(authCode).then( checkResult => {
                if(!checkResult){ // 存在未授权的权限，重新拉起授权
                    login();
                    return;
                }
                resolve();
            }).catch(() => {
                reject();
            })
        })
        .catch(err => {
            log.error("login error:", err);
            reject();
        });
    })
}

// 数据查询接口
export function getBloodSugarData(success, failure) {
    let param = {
        type: HealthEngine.dataType.DATA_BLOODSUGAR,
        startTime: 0,
        endTime: Date.now(),
        queryOption: {
            mOrder: 1, // 倒叙
        },
    };
    HealthEngine.executeQuery(param)
        .then((data) => {
            success(data)
            console.log('executeQuery：', data)
        })
        .catch((err) => {
            log.error("execQueryErr：", err);
            failure(err);
        });
}

// 查询最近一条数据
export function getRecentlyBloodSugarData(success, failure) {
    let param = {
        type: HealthEngine.dataType.DATA_BLOODSUGAR,
        startTime: 0,
        endTime: Date.now(),
        queryOption: {
            mLimit: 1, // 查询一条
            mOrder: 1, // 倒叙
        },
    };
    HealthEngine.executeQuery(param)
        .then((data) => {
            success(data)
            console.log('executeQuery', data)
        })
        .catch((err) => {
            log.error("execQueryErr：", err);
            failure(err);
        });
}

// 存储接口
export function saveBloodSugarDatas(param) {
    console.log("saveSamples param", param);
    return new Promise(res => {
        HealthEngine.saveSamples(param).then((data) => {
            res({ resultCode: 0 })
            console.log('saveSamples success' + data);
        }).catch((err) => {
            console.error('saveSamples err' + JSON.stringify(err));
        });
    })

}

// 添加测试数据
export function testSaveBloodSugarDatas() {
    // 调用方式
    let map = [
        { type: window.timeSlot.AFTER_LUNCH, value: 6.8 }
    ];
    let timestemp = new Date().getTime();
    let param = [{
        type: HealthEngine.dataType.DATA_BLOODSUGAR,
        startTime: timestemp,
        endTime: timestemp,
        deviceInfo: {
            deviceUniqueCode: window.device_sn
        },
        metadata: "{\"mIsConfirmed\":false}",
        map
    }]
    HealthEngine.saveSamples(param).then((data) => {
        console.log(data);
    }).catch((err) => {
        console.error(err);
    });

}

// 删除单条数据接口
export function deleteBloodSugarData(param) {
    return new Promise(res => {
        HealthEngine.deleteSample(param).then(data => {
            console.log('删除成功' + data);
            res();
        }).catch(err => {
            console.error('删除失败' + err);
        })
    })
}

// 数据格式转换
export function formatData(data, sn) {
    console.log("data:", data);
    console.log("sn:", sn);
    let list = [];
    let historyDataList = [];
    // 格式转换
    for (let i = 0; i < data.length; i++) {
        let c = data[i];
        if (!c.metadata || JSON.parse(c.metadata)['mIsConfirmed']) { // 越过已确认数据
            continue;
        }
        let newDate = new Date(c.endTime);
        let year = newDate.getFullYear(),
            month = newDate.getMonth() + 1,
            day = newDate.getDate(),
            hours = newDate.getHours() < 10 ? '0' + newDate.getHours() : newDate.getHours(),
            minutes = newDate.getMinutes() < 10 ? '0' + newDate.getMinutes() : newDate.getMinutes();
        let timeSlot = '',
            value = 0;

        // 拿到血糖值和测量时间段
        for (var v in c.values) {
            if (c.values[v]) {
                timeSlot = v * 1;
                value = c.values[v];
                break;
            }
        }
        list.push({
            date: `${year}/${month}/${day}`,
            endTime: c.endTime,
            contents: [{
                value,
                timeSlot,
                deviceInfo: {
                    // 如果查到的deviceUniqueCode被混淆（即包含*），则直接获取设备sn
                    deviceUniqueCode: c.sourceDevice.deviceUniqueCode.indexOf("*") == -1? c.sourceDevice.deviceUniqueCode: sn,
                    deviceName: c.sourceDevice.deviceName
                },
                time: `${hours}:${minutes}`,
                endTime: c.endTime
            }]
        })
    }

    // 转换后的数组相同日期整合
    let dateList = [];
    list.forEach(val => {
        if (!dateList.includes(val.date)) {
            dateList.push(val.date);
        }
    });

    dateList.forEach(d => {
        let num = 0;
        for (let k = 0; k < list.length; k++) {
            let afterV = list[k],
                len = historyDataList.length;
            if (d == afterV.date && num == 0) {
                historyDataList.push(afterV);
                num++
            } else if (d == afterV.date && num != 0) {
                historyDataList[len - 1].contents.push(afterV.contents[0]);
            }
        }
    });
    console.log("formatData:", historyDataList);
    return historyDataList;
}
