/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License"),
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export default {
    // 血糖测量时间
    timeSlot: {
        FASTING: 2008, //空腹
        AFTER_BREAKFAST: 2009, //早餐后
        BEFORE_LUNCH: 2010, //午餐前
        AFTER_LUNCH: 2011, //午餐后
        BEFORE_DINNER: 2012, //晚餐前
        AFTER_DINNER: 2013, //晚餐后
        BEFORE_SLEEP: 2014, //睡前
        AT_NIGHT: 2015, //夜间
        RANDOM: 2106, //随机
    },
    timeSlots: [
        2008, //空腹
        2009, //早餐后
        2010, //午餐前
        2011, //午餐后
        2012, //晚餐前
        2013, //晚餐后
        2014, //睡前
        2015, //夜间
        2106, //随机
    ],
    getTimeSlotIndex: function(_timeSlot) {
        for (let i = 0; i < window.timeSlots.length; i++) {
            if (_timeSlot === window.timeSlots[i]) {
                return i;
            }
        }
        return -1;
    }
}
