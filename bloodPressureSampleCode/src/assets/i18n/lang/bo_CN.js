module.exports = {
    bloodPressure_device_name: 'ཁྲག་གཤེད་བཀོད་ཁྱབ།',
    bloodPressure_bound: 'མཐུད་ཁ་ཟིན་པ།',
    bloodPressure_begin_measure: 'ཚད་ལེན་འགོ་འཛུགས།',
    bloodPressure_usage_guide: 'བེད་སྤྱོད་ཀྱི་ཕྱོགས་སྟོན།',
    bloodPressure_delete_device: 'སྒྲིག་ཆས་བསུབ་པ།',
    bloodPressure_last_measure: 'ཐེངས་སྔོན་མའི་ཚད་འཇལ།',
    bloodPressure_sync_confirm: 'འགྲོས་མཉམ།',
    bloodPressure_blood_pressure: 'ཁྲག་ཤེད།',
    bloodPressure_pulse: 'རྩ་འཕར་ཚད།',
    bloodPressure_pressure_normal: 'རྒྱུན་ལྡན།',
    bloodPressure_pressure_systolic: 'སྙིང་ཁམས་ཀྱི་སྡུད་སྐུམ་པ།',
    bloodPressure_pressure_diastolic: 'སྙིང་ཁམས་ཀྱི་ལྷོད་འབྱེད།',
    bloodPressure_sync_title: 'སྒྲིག་ཆས་ཀྱི་ལོ་རྒྱུས་གཞི་གྲངས་ལུས་རྩལ་དང་བདེ་ཐང་དུ་གོམ་མཉམ་བྱ་རྒྱུ་ཡིན་ནམ།',
    bloodPressure_sync_cancel: 'གོམ་མཉམ་མི་བྱེད།',
    bloodPressure_measure_tips_01: '%1$s. སྐར་མ་ %2$s ལ་ལྷིང་འཇགས་སུ་བསྡད་རྗེས་ཚད་འཇལ་བྱེད་འགོ་བརྩམས་ན་ལེགས།',
    bloodPressure_measure_tips_02: '%1$s. ཕུ་ཐུང་གྱོན་སྐབས། ཕུ་ཐུང་གི་དཀྱིལ་དང་སྙིང་མཉམ་པར་བྱེད་པ།',
    bloodPressure_measure_tips_03: '%1$s. སྒྲིག་ཆས་སྤྱོད་མཁན་ %2$s ཡི་མཐེབ་ཡང་ན་ %3$s ཡི་མཐེབ་གནོན་ནས་ཚད་འཇལ་བྱེད་འགོ་རྩོམ།'
}