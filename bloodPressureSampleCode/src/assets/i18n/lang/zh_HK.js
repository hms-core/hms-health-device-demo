module.exports = {
    bloodPressure_device_name: '血壓計',
    bloodPressure_bound: '已連結',
    bloodPressure_begin_measure: '開始測量',
    bloodPressure_usage_guide: '使用指引',
    bloodPressure_delete_device: '刪除裝置',
    bloodPressure_last_measure: '上次測量',
    bloodPressure_sync_confirm: '同步',
    bloodPressure_blood_pressure: '血壓',
    bloodPressure_pulse: '脈搏率',
    bloodPressure_pressure_normal: '標準',
    bloodPressure_pressure_systolic: '心臟收縮',
    bloodPressure_pressure_diastolic: '心臟舒張',
    bloodPressure_sync_title: '將裝置數據記錄同步至運動健康？',
    bloodPressure_sync_cancel: '不同步',
    bloodPressure_measure_tips_01: '%1$s. 建議靜坐 %2$s 分鐘後再開始測量',
    bloodPressure_measure_tips_02: '%1$s. 佩戴袖帶時，袖帶中間與心臟處於水平位置',
    bloodPressure_measure_tips_03: '%1$s. 輕按裝置用戶 %2$s 鍵或 %3$s 鍵以開始測量'
}