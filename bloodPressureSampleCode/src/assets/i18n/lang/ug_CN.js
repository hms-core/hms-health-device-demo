module.exports = {
    bloodPressure_device_name: 'قان بېسىمنى ئۆلچىگۈچ',
    bloodPressure_bound: 'باغلاندى',
    bloodPressure_begin_measure: 'ئۆلچەشنى باشلاش',
    bloodPressure_usage_guide: 'ئىشلىتىش كۆرسەتمىسى',
    bloodPressure_delete_device: 'ئۈسكۈنە ئۆچۈرۈش',
    bloodPressure_last_measure: 'ئالدىنقى ئۆلچەش',
    bloodPressure_sync_confirm: 'ماسقەدەملەش',
    bloodPressure_blood_pressure: 'قان بېسىمى',
    bloodPressure_pulse: 'سوقۇش',
    bloodPressure_pressure_normal: 'حسب معمول',
    bloodPressure_pressure_systolic: 'انقباضی',
    bloodPressure_pressure_diastolic: 'انبساطی',
    bloodPressure_sync_title: 'ئۈسكۈنىدىكى بۇرۇنقى مەلۇماتنى تەنتەربىيە-ساغلاملىق ئەپىگە ماسقەدەملەمسىز؟',
    bloodPressure_sync_cancel: 'ياق',
    bloodPressure_measure_tips_01: '%1$s. ئۆلچەشنى باشلاشتىن بۇرۇن %2$s مىنۇت تىنچ ئولتۇرۇڭ.',
    bloodPressure_measure_tips_02: '%1$s. تاقىغان يەڭلىك يۈرەككە ئۇدۇل بولسۇن.',
    bloodPressure_measure_tips_03: '%1$s. ئۈسكۈنىدىكى ئىشلەتكۈچى %2$s ياكى %3$s كۇنۇپكىسىنى بېسىپ ئۆلچەڭ.'
}