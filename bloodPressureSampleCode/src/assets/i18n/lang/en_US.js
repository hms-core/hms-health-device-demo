module.exports = {
    bloodPressure_device_name: 'BP Monitor',
    bloodPressure_bound: 'Associated',
    bloodPressure_begin_measure: 'Start Measurement',
    bloodPressure_usage_guide: 'User guide',
    bloodPressure_delete_device: 'Remove device',
    bloodPressure_last_measure: 'Last time',
    bloodPressure_sync_confirm: 'Sync',
    bloodPressure_blood_pressure: 'Blood pressurec',
    bloodPressure_pulse: 'Pulse',
    bloodPressure_pressure_normal: 'Normal',
    bloodPressure_pressure_systolic: 'Systolic',
    bloodPressure_pressure_diastolic: 'Diastolic',
    bloodPressure_measure_tips_01: '%1$s. Sit still for %2$s minutes before starting.',
    bloodPressure_measure_tips_02: '%1$s. The cuff should be worn on your arm so it is level with your heart.',
    bloodPressure_measure_tips_03: '%1$s. Press the %2$s or %3$s button to start.',
    bloodPressure_prepare_measure: 'Prepare for measurement',
    bloodPressure_measure_tips: 'Start the measurement on the equipment side',
    bloodPressure_sync_title: 'Sync historical data on your device to the Health app?',
    bloodPressure_sync_cancel: 'No'
}