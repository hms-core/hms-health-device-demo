module.exports = {
    bloodPressure_device_name: '血壓計',
    bloodPressure_bound: '已連結',
    bloodPressure_begin_measure: '開始測量',
    bloodPressure_usage_guide: '使用指南',
    bloodPressure_delete_device: '刪除裝置',
    bloodPressure_last_measure: '上次測量',
    bloodPressure_sync_confirm: '同步',
    bloodPressure_blood_pressure: '血壓',
    bloodPressure_pulse: '脈率',
    bloodPressure_pressure_normal: '正常',
    bloodPressure_pressure_systolic: '收縮',
    bloodPressure_pressure_diastolic: '	舒張',
    bloodPressure_sync_title: '是否將裝置記錄資料同步至運動健康 App？',
    bloodPressure_sync_cancel: '不同步',
    bloodPressure_measure_tips_01: '%1$s. 建議靜坐 %2$s 分鐘後開始測量',
    bloodPressure_measure_tips_02: '%1$s. 佩戴袖帶時，袖帶中間與心臟等高',
    bloodPressure_measure_tips_03: '%1$s. 依不同裝置使用者，按 %2$s 鍵或 %3$s 鍵開始測量'
}