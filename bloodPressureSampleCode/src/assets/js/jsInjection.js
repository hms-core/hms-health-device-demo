/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import log from './log.js'

export default {
    getLocale() {
        let data = ''
        try {
            data = BasicLib.option.getLocale(); 
        } catch (e) {
            log.info('getLocale error' + e);
        }
        return data;
    },

    /**
     * 获取设备的deviceId
     */
    getDeviceId() {
        var data = "";
        log.info("request native interface: getDeviceId");
        try {
            data = window.hilink.getDeviceId();
        } catch (e) {
            log.info('hilink error: ' + e);
        }
        return data;
    },

    /**
     * 获取设备信息
     */
     getDeviceInfo(callback) {
        log.info("request native interface: getDeviceInfo");
        try {
            window.hilink.getDeviceInfo(callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    // 删除设备
    unbindDevice(callback){
        log.info("request native interface: unbindDevice");
        try {
            window.hilink.unbindDevice(callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    // 绑定设备
    bindDevice(data,callback){
        log.info("request native interface: bindDevice");
        try {
            window.hilink.bindDevice(data, callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    }
}