/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import scopes from './scopes';
import request from './request';

// 登录授权接口
export function login() {
    return new Promise((resolve, reject) => {
        let useAuthCode = true;
        BasicLib.account.signIn({
            appid: globalData.APP_ID, // 联盟上申请到的APPID
            useAuthCode,
            scopes
        })
        .then(res => {
            log.info('login success', res);
            // AT模式
            if(!useAuthCode){
                resolve();
                return;
            }

            // AuthCode模式
            request.checkScopes(res.serverAuthCode).then( checkResult => {
                if(!checkResult){ // 存在未授权的权限，重新拉起授权
                    login();
                    return;
                }
                resolve();
            }).catch(() => {
                reject();
            })
        })
        .catch(err => {
            log.error("login error:", err);
            reject();
        });
    })
}
