/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const state = {
    accessToken: "",
    sampleData: {
        // 血压数据
        highPressure: null, //高压
        lowPressure: null, //低压
        pulseRate: null, //脉率
    },
    isHaveData: false, // 是否存在数据
    measureTime: '', // 最近测量时间
    syncData: false, // 是否同步数据
    bpCategory: 0, // 血压分级  0：偏低 1：正常，2：正常高值，3：轻度，4：中度，5：重度
}


export default state;