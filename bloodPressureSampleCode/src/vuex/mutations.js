/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const mutations = {
    // 获取凭证
    getAccessToken(state,data) {
        console.log("getAccessToken:",data);
        state.accessToken = data;
    },
    
    // 存储最近一套血压数据
    getQuery(state,data){
        state.sampleData = {
            highPressure: data['2006'] || '--',
            lowPressure: data['2007'] || '--',
            pulseRate: data['2018'] || '--'
        }
        state.isHaveData = true;
    },

    // 最近一次测量时间
    getTime(state,data){
        state.measureTime = data;
    },

    // 同步状态
    changeSyncData(state,data){
        state.syncData = data;
    },

    // 血压级别判断
    getBpCategory(state,data){
        state.bpCategory = data;
    }
}

export default mutations;