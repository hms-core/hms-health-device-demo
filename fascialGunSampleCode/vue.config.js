module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "./" : "./",
  outputDir: "dist",
  assetsDir: "static",
  lintOnSave: false, // 是否开启eslint保存检测
  productionSourceMap: false, // 是否在构建生产包时生成sourcdeMap
  devServer: {
    // host: "localhost",
    host: "0.0.0.0", //局域网和本地访问
    port: "8090",
    hot: true,
    /* 自动打开浏览器 */
    open: false,
    overlay: {
      warning: false,
      error: false
    }
  },
  configureWebpack: {
    //关闭 webpack 的性能提示
    performance: {
      hints:false
    }
  }
}
