/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//  发送的指令集合
export default {
    start_directive: "00000000001",
    pause_directive: "00000000002",
    // 档位
    first_gear_directive: "10000000001",
    second_gear_directive: "10000000002",
    third_gear_directive: "10000000003",
    fourth_gear_directive: "10000000004",
    // 模式选择
    default_directive: "20000000001",
    shoulder_directive: "20000000002",
    arm_directive: "20000000003",
    loin_directive: "20000000004",
    buttock_directive: "20000000005",
    leg_directive: "20000000006",
    foot_directive: "20000000007",
    //关机
    shutdown_directive: "30000000001",
    //定时
    one_timer_directive: "40000000001",
    ten_timer_directive: "40000000002"
}