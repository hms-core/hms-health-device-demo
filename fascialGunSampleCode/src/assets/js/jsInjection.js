/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import log from './log.js'

export default {
    getLocale() {
        let data = ''
        try {
            data = BasicLib.option.getLocale(); 
        } catch (e) {
            log.info('getLocale error' + e);
        }
        return data;
    },

	setTitle(data) {
        document.title = data;
        try {
            BasicLib.container.setPageTitle(data);
        } catch (e) {
            log.info('JsInteraction error: ' + e);
        }
    },

    /**
     * 获取设备的deviceId
     */
    getDeviceId() {
        var data = "";
        log.info("request native interface: getDeviceId");
        try {
            data = window.hilink.getDeviceId();
        } catch (e) {
            log.info('hilink error: ' + e);
        }
        return data;
    },

    /**
     * 获取设备信息
     */
     getDeviceInfo(callback) {
        log.info("request native interface: getDeviceInfo");
        try {
            window.hilink.getDeviceInfo(callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 创建蓝牙连接
     */
    createBleConnection(deviceId) {
        log.info("request native interface: createBleConnection");
        try {
            window.hilink.createBleConnection(deviceId);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 监听蓝牙连接状态
     */
    onBleConnectionStateChange(callback) {
        log.info("request native interface: onBleConnectionStateChange");
        try {
            window.hilink.onBleConnectionStateChange(callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 监听服务发现的变化
     */
    onBleServicesDiscovered(callback) {
        log.info("request native interface: onBleServicesDiscovered");
        try {
            window.hilink.onBleServicesDiscovered(callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 使设备主动上报数据
     */
    notifyBleCharacteristicValueChange(deviceId, serviceId, characteristicId, flag) {
        log.info("request native interface: notifyBleCharacteristicValueChange");
        try {
            return window.hilink.notifyBleCharacteristicValueChange(deviceId, serviceId, characteristicId, flag);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 监听特征值的变化
     */
    onBleCharacteristicValueChange(callback) {
        log.info("request native interface: onBleConnectionStateChange");
        try {
            window.hilink.onBleCharacteristicValueChange(callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 断开蓝牙连接
     */
    closeBleConnection(deviceId) {
        log.info("request native interface: closeBleConnection");
        try {
            window.hilink.closeBleConnection(deviceId);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 判断手机蓝牙状态
     */
    getBluetoothAdapterState(callback) {
        log.info("request native interface: getBluetoothAdapterState");
        try {
            window.hilink.getBluetoothAdapterState(callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    //写入指令
    writeBLECharacteristicValue(deviceId, serviceId, characteristicId, data, calback){
        log.info("request native interface: writeBLECharacteristicValue");
        try {
            window.hilink.writeBLECharacteristicValue(deviceId, serviceId, characteristicId, data, calback)
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 写数据的回调方法
     *
     * @param {Object} response 返回值
     */
    writeCharacteristicCallback(response) {
        // 解析写数据的结果
        let result = JSON.parse(response);
        if (result && result.errCode == 0) {
            console.info("writeCharacteristicCallback success: " + response);
        } else {
            // 时间同步失败
            console.warn('writeCharacteristicCallback fail: ' + response);
        }
    },

    /**
     * 写指令
     * @param {string} data  写入的指令
    */
    writeDirective(deviceId, serviceId, characteristicId, data){
        window.writeCharacteristicCallback = this.writeCharacteristicCallback;
        this.writeBLECharacteristicValue( deviceId, serviceId, characteristicId, data, "writeCharacteristicCallback" );
    },

    // 删除设备
    unbindDevice(callback){
        log.info("request native interface: unbindDevice");
        try {
            window.hilink.unbindDevice(callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    }
}