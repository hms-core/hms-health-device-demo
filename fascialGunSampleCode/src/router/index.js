/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Vue from "vue"
import Router from "vue-router"

Vue.use(Router)


const MainView = ()=>import("../views/mainView.vue");
const Upgrade = ()=>import("../views/upgrade.vue");
const AiRecommend = ()=>import("../views/aiRecommend.vue");
const ModeView = ()=>import("../views/modeView.vue");
const MassageRecords = ()=>import("../views/massageRecords.vue");
const VersionView = ()=>import("../views/versionView.vue");

export default new Router({
    routes: [
        {
            path: '/',
            name: 'mainView',
            component: MainView,
        },
        {   // 固件升级
            path: '/upgrade',
            name: 'Upgrade',
            component: Upgrade
        },
        {   // 课程推荐
            path: '/aiRecommend',
            name: 'AiRecommend',
            component: AiRecommend
        },
        {   // 模式选择
            path: '/modeView',
            name: 'ModeView',
            component: ModeView
        },
        {   // 按摩记录
            path: '/massageRecords',
            name: 'MassageRecords',
            component: MassageRecords
        },
        {   // 新版本
            path: '/versionView',
            name: 'VersionView',
            component: VersionView
        }
    ]
})
