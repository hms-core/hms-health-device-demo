/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const mutations = {
    // 模式选择
    chooseModeMethod(state, n = 0) {
        const list = state.storeModeList;
        state.modeIndex = n;
        list.forEach(val=>{
            val.modeLight = false;
        })
        list[n].modeLight = list[n].modeLight || !list[n].modeLight;
        
        state.runStatus = true;
    },

    // 状态变化
    changeConnectStatus(state,data){
        state.connectStatus = data;
        state.connected = state.connectStatus == 2;
    },

    // 获取设备Id
    getDeviceId(state,data){
        state.deviceId = data;
    },

    // 运行切换
    runStatusMutations(state){
        state.runStatus = !state.runStatus;
    },

    // 档位切换
    changeGear(state,index){
        state.gearOptionList.forEach((val,i)=>{
            val.actived = false;
            if(i == index){
                val.actived = !val.actived;
            }
        });
    },

    // 按摩定时
    changeTimer(state,index){
        state.timerOptionList.forEach((val,i)=>{
            val.actived = false;
            if(i == index){
                val.actived = !val.actived;
            }
        });
    }
}

export default mutations;