
/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const state = {
    connected: false, // 是否连接成功
    connectStatus: null, // 连接状态  0 未连接； 1 连接中； 2 已连接
    modeIndex: 0, // 模式选择下标
    deviceId: '', //设备id，用于蓝牙连接
    runStatus: false, //运行状态  false 未运行； true 已运行
    storeModeList: [
        {
            icon: require("@/assets/image/mode_btn_default.png"),
            icon_on: require("@/assets/image/mode_btn_default_on.png"),
            icon_dark: require("@/assets/image/dark/mode_btn_default_dark.png"),
            name: "默认",
            modeLight: true
        },
        {
            icon: require("@/assets/image/mode_btn_shoulder.png"),
            icon_on: require("@/assets/image/mode_btn_shoulder_on.png"),
            icon_dark: require("@/assets/image/dark/mode_btn_shoulder_dark.png"),
            name: "肩部",
            modeLight: false
        },
        {
            icon: require("@/assets/image/mode_btn_arm.png"),
            icon_on: require("@/assets/image/mode_btn_arm_on.png"),
            icon_dark: require("@/assets/image/dark/mode_btn_arm_dark.png"),
            name: "手臂部",
            modeLight: false
        },
        {
            icon: require("@/assets/image/mode_btn_loin.png"),
            icon_on: require("@/assets/image/mode_btn_loin_on.png"),
            icon_dark: require("@/assets/image/dark/mode_btn_loin_dark.png"),
            name: "腰部",
            modeLight: false
        },
        {
            icon: require("@/assets/image/mode_btn_buttock.png"),
            icon_on: require("@/assets/image/mode_btn_buttock_on.png"),
            icon_dark: require("@/assets/image/dark/mode_btn_buttock_dark.png"),
            name: "臀部",
            modeLight: false
        },
        {
            icon: require("@/assets/image/mode_btn_leg.png"),
            icon_on: require("@/assets/image/mode_btn_leg_on.png"),
            icon_dark: require("@/assets/image/dark/mode_btn_leg_dark.png"),
            name: "腿部",
            modeLight: false
        },
        {
            icon: require("@/assets/image/mode_btn_foot.png"),
            icon_on: require("@/assets/image/mode_btn_foot_on.png"),
            icon_dark: require("@/assets/image/dark/mode_btn_foot_dark.png"),
            name: "脚部",
            modeLight: false
        }
    ],
    gearOptionList: [  //档位  默认一档
        {
            value: '1档',
            actived: true
        },{
            value: '2档',
            actived: false
        },{
            value: '3档',
            actived: false
        },{
            value: '4档',
            actived: false
        }
    ],
    timerOptionList: [ //按摩定时
        {
            value: '5分钟',
            actived: false
        },{
            value: '10分钟',
            actived: false
        },{
            value: '15分钟',
            actived: false
        },{
            value: '20分钟',
            actived: false
        },{
            value: '25分钟',
            actived: false
        },
    ]
}


export default state;