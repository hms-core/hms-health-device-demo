import Vue from "vue"
import Router from "vue-router"

const MainView = ()=>import('../views/mainView.vue')

Vue.use(Router)

export default new Router({
    routes: [             
        {
            path: '/',        
            name: 'MainView',
            component: MainView
        },
    ]
})
