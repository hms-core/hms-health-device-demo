/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import log from './log.js'
var oneMinuteTime = 60000;
var oneDayTime = oneMinuteTime * 60 * 24;
var savingTime = Date.now() - oneDayTime;
export default {
    // 获取语言标识
    getLocale() {
        let data = ''
        try {
            data = BasicLib.option.getLocale();
        } catch (e) {
            log.info('getLocale error' + e);
        }
        return data;
    },

    // 获取设备信息
    deviceInfo() {
        let data = ''
        try {
            data = BasicLib.util.deviceInfo();
        } catch (e) {
            log.info('deviceInfo error' + e);
        }
        return data;
    },

    // 切换渲浸模式和状态栏字体颜色
    setImmerse(data) {
        try {
            BasicLib.container.setImmerse(data);
        } catch (e) {
            log.info('setImmerse error' + e);
        }
    },

    /**
     * 获取设备的deviceId
     */
    getDeviceId() {
        var data = "";
        log.info("request native interface: getDeviceId");
        try {
            data = window.hilink.getDeviceId();
        } catch (e) {
            log.info('hilink error: ' + e);
        }
        return data;
    },

    /**
     * 创建蓝牙连接
     */
    createBleConnection(deviceId) {
        log.info("request native interface: createBleConnection");
        try {
            window.hilink.createBleConnection(deviceId);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 监听蓝牙连接状态
     */
    onBleConnectionStateChange(callback) {
        log.info("request native interface: onBleConnectionStateChange");
        try {
            window.hilink.onBleConnectionStateChange(callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 监听服务发现的变化
     */
    onBleServicesDiscovered(callback) {
        log.info("request native interface: onBleServicesDiscovered");
        try {
            window.hilink.onBleServicesDiscovered(callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 使设备主动上报数据
     */
    notifyBleCharacteristicValueChange(deviceId, serviceId, characteristicId, flag) {
        log.info("request native interface: notifyBleCharacteristicValueChange");
        try {
            return window.hilink.notifyBleCharacteristicValueChange(deviceId, serviceId, characteristicId, flag);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 监听特征值的变化
     */
    onBleCharacteristicValueChange(callback) {
        log.info("request native interface: onBleConnectionStateChange");
        try {
            window.hilink.onBleCharacteristicValueChange(callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 断开蓝牙连接
     */
    closeBleConnection(deviceId) {
        log.info("request native interface: closeBleConnection");
        try {
            window.hilink.closeBleConnection(deviceId);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 判断手机蓝牙状态
     */
    getBluetoothAdapterState(callback) {
        log.info("request native interface: getBluetoothAdapterState");
        try {
            window.hilink.getBluetoothAdapterState(callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    //写入指令
    writeBLECharacteristicValue(deviceId, serviceId, characteristicId, data, calback){
        log.info("request native interface: writeBLECharacteristicValue");
        try {
            window.hilink.writeBLECharacteristicValue(deviceId, serviceId, characteristicId, data, calback)
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    /**
     * 写数据的回调方法
     *
     * @param {Object} response 返回值
     */
    writeCharacteristicCallback(response) {
        // 解析写数据的结果
        let result = JSON.parse(response);
        if (result && result.errCode == 0) {
            console.info("writeCharacteristicCallback success: " + response);
        } else {
            console.warn('writeCharacteristicCallback fail: ' + response);
        }
    },

    /**
     * 写指令
     * @param {string} data  写入的指令
    */
    writeDirective(deviceId, serviceId, characteristicId, data){
        window.writeCharacteristicCallback = this.writeCharacteristicCallback;
        this.writeBLECharacteristicValue( deviceId, serviceId, characteristicId, data, "writeCharacteristicCallback" );
    },

    // 删除设备
    unbindDevice(callback){
        log.info("request native interface: unbindDevice");
        try {
            window.hilink.unbindDevice(callback);
        } catch (e) {
            log.info('hilink error: ' + e);
        }
    },

    // 查询所有数据
    getTemperatureData() {
        let param = {
            type: HealthEngine.dataType.DATA_BODYTEMPERATURE,
            startTime: 0,
            endTime: Date.now(),
        };
        return new Promise(res => {
            HealthEngine.execQuery(param)
            .then(data => {
                console.log('execQuery success', data);
                res(data);
            })
            .catch(err => {
                console.error("execQuery err", err);
            });
        })
        
    },

    // 存储单条数据
    saveTemperatureData() {
        let param = {
            type: HealthEngine.dataType.DATA_BODYTEMPERATURE,
            startTime: Date.now(),
            endTime: Date.now(),
            value: 40,
        };
        return new Promise( res => {
            HealthEngine.saveSample(param)
            .then(data => {
                res({ resultCode: 0 })
                console.log('saveSample success', data);
            })
            .catch(err => {
                console.error('saveSample err', JSON.stringify(err));
            });
        })
    },

    // 存储多条数据
    saveTemperatureDatas() {
        let param = [{
            type: HealthEngine.dataType.DATA_BODYTEMPERATURE,
            startTime: Date.now(),
            endTime: Date.now(),
            value: 40
        },{
            type: HealthEngine.dataType.DATA_BODYTEMPERATURE,
            startTime: savingTime,
            endTime: savingTime,
            value: 38
        }];
        return new Promise( (res, rej) => {
            HealthEngine.saveSamples(param)
            .then(data => {
                res({ resultCode: 0 });
                console.log('saveSamples success', data);
            })
            .catch(err => {
                rej(err);
                console.error('saveSamples err', JSON.stringify(err));
            });
        })
    }
}