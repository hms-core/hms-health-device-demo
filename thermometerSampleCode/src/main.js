import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router/index.js'
import BasicLib from '@huawei/h5pro-basic-lib';
import HealthEngine from '@huawei/h5pro-health-engine';
import VueI18n from 'vue-i18n';
import store from './vuex/store';
import jsInjection from '@/assets/js/jsInjection.js';
import log from './assets/js/log.js';
import globalData from '@/assets/js/global.js';
import directiveData from './assets/js/directives';
import '@huawei/h5pro-hilink';

Vue.use(VueI18n);

window.BasicLib = BasicLib;
window.HealthEngine = HealthEngine;
window.log = log;
Vue.prototype.jsInjection = jsInjection;
Vue.prototype.globalData = globalData;
Vue.prototype.directiveData = directiveData;

// i-18n
const messages = {
    'bo_CN': require('./assets/i18n/lang/bo_CN'),
    'en_US': require('./assets/i18n/lang/en_US'),
    'ug_CN': require('./assets/i18n/lang/ug_CN'),
    'zh_CN': require('./assets/i18n/lang/zh_CN'),
    'zh_HK': require('./assets/i18n/lang/zh_HK'),
    'zh_TW': require('./assets/i18n/lang/zh_TW'),
}
const defaultLocale = 'zh_CN'
var locale = jsInjection.getLocale() // 语言标识
Vue.prototype.$rtl = ['ug_CN', 'ug'].includes(locale);

var localeFounded = false
for (var prop in messages) {
    if (locale === prop) {
        localeFounded = true
        break
    }
}
const defaultLanguageConfig = {
    'en': 'en_US',
    'es': 'es_ES',
    'pt': 'pt_BR',
    'sr': 'sr_MS'
}
// 只根据语言进行匹配
if (!localeFounded) {
    var language = locale.split('_')[0]
    // 遍历以上4种存在默认语言的语种
    for (prop in defaultLanguageConfig) {
        if (prop === language) {
            locale = defaultLanguageConfig[prop]
            localeFounded = true
            break
        }
    }
    // 从messages中遍历查找语言
    for (prop in messages) {
        if (localeFounded) {
            break
        }
        var configLanguage = prop.split('_')[0]
        if (configLanguage === language) {
            locale = prop
            localeFounded = true
        }
    }
}

// 仍然没有找到匹配的语言，使用默认语言en_US
if (!localeFounded) {
    locale = defaultLocale
}
log.info('locale = ' + locale)
const i18n = new VueI18n({
    locale: locale,
    messages: messages
})


async function obtainInfo() {
    // 获取设备信息，包括是否是暗色模式
    if (process.env.NODE_ENV == "development") {
        window.isDark = false;
        window.topHeight = 0;
    } else {
        const currentDeviceInfo = await jsInjection.deviceInfo();
        console.info(currentDeviceInfo);
        window.isDark = currentDeviceInfo.isDarkTheme;
        window.topHeight = currentDeviceInfo.topLayout;
    }
    // 隐藏标题栏
    jsInjection.setImmerse({
        isImmerse: true,
        isStatusBarTextBlack: true
    });

    Vue.config.devtools = true;
    Vue.use(VueRouter)
    new Vue({
        render: h => h(App),
        router,
        store,
        i18n
    }).$mount('#app')
}

obtainInfo();